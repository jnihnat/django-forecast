from django.db import models
from django.utils import timezone
import datetime


class Country(models.TextChoices):
    CZ = "Czech Republic"
    UK = "United Kingdom"
    SK = "Slovak Republic"


class Forecast(models.Model):
    """
    model for saving forecast data
    """

    forecast_date = models.DateField("Date of forecast", max_length=50)
    country_code = models.CharField(
        "Country of forecast", max_length=15, choices=Country.choices
    )
    average_temp = models.DecimalField(
        "Average Temperature", decimal_places=2, max_digits=20
    )
    last_updated = models.DateField("Last updated", default=timezone.now)

    def save(self, *args, **kwargs):
        if self.last_updated:
            self.last_updated = datetime.date.today()
        return super(Forecast, self).save(*args, **kwargs)
