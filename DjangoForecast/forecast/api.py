import requests
from django.conf import settings


def get_forecast(country):
    payload = {
        "aqi": "no",
        "alerts": "no",
        "days": "10",
        "q": country,
        "key": settings.FORECAST_KEY,
    }
    return requests.get("http://api.weatherapi.com/v1/forecast.json", params=payload)
