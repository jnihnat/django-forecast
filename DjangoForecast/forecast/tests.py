from datetime import date, timedelta
from django.urls import reverse
from django.test import TestCase
from .models import Country, Forecast

# Create your tests here.
class WeatherForecastTests(TestCase):
    def setUp(self):
        self.today = date.today().strftime("%Y-%m-%d")
        self.far_date = (date.today() + timedelta(days=4)).strftime("%Y-%m-%d")
        forecast_today = Forecast.objects.create(
            forecast_date=self.today,
            average_temp=20.5,
            country_code=Country.CZ,
            last_updated=date.today(),
        )

    def test_get_forecast_good(self):
        url = reverse("forecast")
        url += "?country_code=CZ&date=" + self.today
        response = self.client.get(url, format="json")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), {"forecast": "good"})

    def test_get_forecast_today_UK(self):
        url = reverse("forecast")
        url += "?country_code=UK&date=" + self.today
        response = self.client.get(url, format="json")
        self.assertEqual(response.status_code, 200)

    def test_date_and_country_empty(self):
        url = reverse("forecast")
        response = self.client.get(url, format="json")
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.json(), {"error": "Date or Country_Code is empty"})

    def test_incorrect_country(self):
        url = reverse("forecast")
        url += "?country_code=DE&date=" + self.today
        response = self.client.get(url, format="json")
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.json(), {"error": "Country code not found, pick from CZ,UK, SK"}
        )

    def test_incorect_date_format(self):
        url = reverse("forecast")
        url += "?country_code=SK&date=22-07-2021"
        response = self.client.get(url, format="json")
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.json(), {"error": "Incorrect date format, should be YYYY-MM-DD"}
        )

    def test_date_to_far(self):
        url = reverse("forecast")
        url += "?country_code=CZ&date=" + self.far_date
        response = self.client.get(url, format="json")
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.json(), {"error": "Date is to far from today"})
