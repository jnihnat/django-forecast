from .api import get_forecast
from .models import Forecast, Country
from .serializers import ForecastUpdateSerializer


def update_forecast(country):
    forecast = get_forecast(country)

    for item in forecast.json()["forecast"]["forecastday"]:
        instance = Forecast.objects.filter(
            country_code=country, forecast_date__startswith=item["date"],
        ).first()
        if not instance:
            forecast_serializer = ForecastUpdateSerializer(data=item)
        else:
            forecast_serializer = ForecastUpdateSerializer(instance, data=item)
        forecast_serializer.is_valid(raise_exception=True)
        forecast_serializer.validated_data["country_code"] = country
        forecast_serializer.save()
