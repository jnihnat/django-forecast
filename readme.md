# Django Forecast app

Django forecast app is app for obtaining info if forecast for comming days will be good or no

## Installation

Run commands from folder DjangoForecast
```bash
pip install -r requirements_dev
python manage.py migrate
python manage.py runserver
```

## Usage

- to get forecast info call **http://localhost:8000/weather-forecast/?country_code=CZ&date=2021-07-22**
- where country_code needs to be one of "CZ", "SK", "UK" and date needs to be in format YYYY-MM-DD

## How to run tests
- run in bash command: 
```
python manage.py test 
```

