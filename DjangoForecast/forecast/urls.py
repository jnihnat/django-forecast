from django.urls import path

from .views import forecast_view

urlpatterns = [
    path("weather-forecast/", forecast_view, name="forecast"),
]
