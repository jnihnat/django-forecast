from copy import Error
from datetime import datetime, date, timedelta
from django.core.management.base import BaseCommand, CommandError
from forecast.models import Forecast, Country
from forecast.scraper import update_forecast
import argparse


def valid_date(s):
    today = date.today().strftime("%Y-%m-%d")
    today = datetime.strptime(today, "%Y-%m-%d")
    try:
        requested_date_datetime = datetime.strptime(s, "%Y-%m-%d")
    except ValueError:
        msg = "Not a valid date: '{0}'.".format(s)
        raise argparse.ArgumentTypeError(msg)
    if requested_date_datetime >= (today + timedelta(days=3)):
        msg = "Date to far to the future: '{0}'.".format(s)
        raise argparse.ArgumentTypeError(msg)
    else:
        return requested_date_datetime


def valid_country(s):
    try:
        return Country[s]
    except:
        msg = "Not a valid country code: '{0}'.".format(s)
        raise argparse.ArgumentTypeError(msg)


class Command(BaseCommand):
    help = "Get the weather information"

    def add_arguments(self, parser):
        parser.add_argument("date", type=valid_date)
        parser.add_argument("country", type=valid_country)

    def handle(self, *args, **options):
        instance = Forecast.objects.filter(
            country_code=options["country"],
            forecast_date__startswith=options["date"].strftime("%Y-%m-%d"),
            last_updated__startswith=date.today().strftime("%Y-%m-%d"),
        ).first()
        if not instance:
            update_forecast(options["country"])
            instance = Forecast.objects.filter(
                country_code=options["country"],
                forecast_date__startswith=options["date"].strftime("%Y-%m-%d"),
                last_updated__startswith=date.today().strftime("%Y-%m-%d"),
            ).first()

        if not instance:
            msg = "Sorry we don't have forecast for given date: '{0}'.".format(
                options["date"].strftime("%Y-%m-%d")
            )
            raise Error(msg)

        if instance.average_temp > 20:
            json_object = {"forecast": "good"}
        elif instance.average_temp < 20 and instance.average_temp > 10:
            json_object = {"forecast": "soso"}
        else:
            json_object = {"forecast": "bad"}
        self.stdout.write(self.style.SUCCESS(json_object))
