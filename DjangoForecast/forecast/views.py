from django.db.models.base import Model
from django.shortcuts import render
from django.http import JsonResponse
from .scraper import update_forecast
from .models import Country, Forecast
import datetime
from django.utils import timezone


def forecast_view(request):
    requested_country = request.GET.get("country_code", "")
    requested_date = request.GET.get("date", "")
    today = datetime.date.today().strftime("%Y-%m-%d")
    today = datetime.datetime.strptime(today, "%Y-%m-%d")

    if requested_date == "" or requested_country == "":
        json_object = {"error": "Date or Country_Code is empty"}
        return JsonResponse(json_object, status=400)

    try:
        obj_country = Country[requested_country]
    except:
        json_object = {"error": "Country code not found, pick from CZ,UK, SK"}
        return JsonResponse(json_object, status=400)

    try:
        requested_date_datetime = datetime.datetime.strptime(requested_date, "%Y-%m-%d")
    except ValueError:
        json_object = {"error": "Incorrect date format, should be YYYY-MM-DD"}
        return JsonResponse(json_object, status=400)

    else:
        if requested_date_datetime >= (today + datetime.timedelta(days=3)):
            json_object = {"error": "Date is to far from today"}
            return JsonResponse(json_object, status=400)
    instance = Forecast.objects.filter(
        country_code=obj_country,
        forecast_date__startswith=requested_date,
        last_updated__startswith=today.strftime("%Y-%m-%d"),
    ).first()
    if not instance and requested_date_datetime >= today:
        update_forecast(obj_country)

    instance = Forecast.objects.filter(
        country_code=obj_country, forecast_date__startswith=requested_date,
    ).first()

    if not instance:
        json_object = {"error": "Sorry we don't have forecast for given date"}
        return JsonResponse(json_object, status=400)

    if instance.average_temp > 20:
        json_object = {"forecast": "good"}
    elif instance.average_temp < 20 and instance.average_temp > 10:
        json_object = {"forecast": "soso"}
    else:
        json_object = {"forecast": "bad"}
    return JsonResponse(json_object)
