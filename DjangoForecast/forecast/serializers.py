from rest_framework import serializers
from .models import Forecast


class DayNestedSerializer(serializers.Serializer):
    """
    serializer for nested object "day" in forecast serializer
    """

    avgtemp_c = serializers.FloatField()

    class Meta:
        fields = ("avgtemp_c",)


class ForecastUpdateSerializer(serializers.ModelSerializer):
    """
    serializer for forecast from weatherapi
    """

    date = serializers.DateField(source="forecast_date")
    day = DayNestedSerializer()

    class Meta:
        model = Forecast
        fields = (
            "date",
            "day",
        )

    def create(self, validated_data, **kwargs):
        self.validated_data["average_temp"] = self.validated_data.pop("day")[
            "avgtemp_c"
        ]
        return super().create(self.validated_data)

    def update(self, instance, validated_data):
        self.validated_data["average_temp"] = self.validated_data.pop("day")[
            "avgtemp_c"
        ]
        return super().update(instance, validated_data)

